package tdd.training.mra;

import java.util.Arrays;
import java.util.List;

public class MarsRover {
	
	private int planetx;
	private int planety;
	private Integer[][] planet;
	private int roverx;
	private int rovery;
	private String roverf;
	
	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		
		this.planetx=planetX;
		this.planety=planetY;
		
		this.roverx=0;
		this.rovery=0;
		this.roverf="N";
		
		this.planet = new Integer[planetX][planetY];
		
		for(int i=0; i<planetObstacles.size(); i++) {
		
			String ostacolo= planetObstacles.get(i);
			String[] posostacolo = ostacolo.split(",");
			posostacolo[0] = posostacolo[0].replaceAll("\\D+","");
			posostacolo[1] = posostacolo[1].replaceAll("\\D+","");
			this.planet[Integer.valueOf(posostacolo[0])][Integer.valueOf(posostacolo[1])]=1;
			
		}
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		if(this.planet[x][y]==1) {
			return true;
		}
		return false;
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		for(int i = 0; i < commandString.length(); i++){
			String c = String.valueOf(commandString.charAt(i));
			if(c.equals("l")) {
				this.roverf="W";
			}
			if(c.equals("r")) {
				this.roverf="E";
			}
			
			if(c.equals("f")) {
				if(this.roverf=="N") {
					this.rovery= this.rovery+1;}
				if(this.roverf=="S") {
					this.rovery= this.rovery-1;}
				if(this.roverf=="E") {
					this.roverx= this.roverx+1;}
				if(this.roverf=="O") {
					this.roverx= this.roverx-1;}
			}
			if(c.equals("b")) {
				if(this.roverf=="N") {
					this.rovery= this.rovery-1;}
				if(this.roverf=="S") {
					this.rovery= this.rovery+1;}
				if(this.roverf=="E") {
					this.roverx= this.roverx-1;}
				if(this.roverf=="O") {
					this.roverx= this.roverx+1;}
			}
		}	
		planetBoundaryCheck();
		String roverposition= "("+roverx+","+rovery+","+roverf+")";
		return roverposition;
	}
	
	private void planetBoundaryCheck() {
		if(this.roverx>planetx-1) {
			this.roverx= this.roverx-planetx;
		}
		if(this.roverx<0) {
			this.roverx= this.roverx+planetx;
		}
		if(this.rovery>planety-1) {
			this.rovery= this.rovery-planety;
		}
		if(this.rovery<0) {
			this.rovery= this.rovery+planety;
		}
		
	}
}
