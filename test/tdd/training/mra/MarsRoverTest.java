package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {
	
	public MarsRover roverfixture() throws Exception{
		List<String> ostacoli = new ArrayList<String>();
		ostacoli.add("(0,1)");
		ostacoli.add("(2,2)");
		MarsRover mr= new MarsRover(10,10,ostacoli);
		return mr; 
	}
	
	@Test
	public void ObstacleTest() throws Exception{
		MarsRover mr= roverfixture();
		assertEquals(true, mr.planetContainsObstacleAt(0,1));
	}
	@Test
	public void ObstacleTest2() throws Exception{
		MarsRover mr= roverfixture();
		assertEquals(true, mr.planetContainsObstacleAt(2,2));
	}
	
	@Test
	public void RoverEmptyCommandTest() throws Exception{
		MarsRover mr=roverfixture();
		assertEquals("(0,0,N)", mr.executeCommand(""));
	}
	
	@Test
	public void RoverTurningLeftTest() throws Exception{
		MarsRover mr=roverfixture();
		assertEquals("(0,0,W)", mr.executeCommand("l"));
	}
	
	@Test
	public void RoverTurningRightTest() throws Exception{
		MarsRover mr=roverfixture();
		assertEquals("(0,0,E)", mr.executeCommand("r"));
	}
	
	@Test
	public void RoverForwardTest() throws Exception{
		MarsRover mr=roverfixture();
		assertEquals("(0,1,N)", mr.executeCommand("f"));
	}
	
	@Test
	public void RoverBackwardTest() throws Exception{
		MarsRover mr=roverfixture();
		mr.executeCommand("f");
		assertEquals("(0,0,N)", mr.executeCommand("b"));
	}
	
	@Test
	public void RoverCombinedCommandTest() throws Exception{
		List<String> ostacoli = new ArrayList<String>();
		MarsRover mr= new MarsRover(10,10,ostacoli);
		assertEquals("(2,2,E)",mr.executeCommand("ffrff"));
	}
	@Test
	public void RoverSpherePlanetTest() throws Exception{
		MarsRover mr=roverfixture();
		assertEquals("(0,9,N)",mr.executeCommand("b"));
	}
	
}
